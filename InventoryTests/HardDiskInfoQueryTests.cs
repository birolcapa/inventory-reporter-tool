﻿using System.Collections.Generic;
using Inventory;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InventoryTests
{
    [TestClass]
    public class HardDiskInfoQueryTests
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(HardDiskInfoQueryTests));

        [TestMethod]
        public void get_harddisks()
        {
            List<HardDiskType> harddisks = new HardDiskInfo().HardDisks;
            Assert.AreNotEqual(0, harddisks.Count);
            foreach (var item in harddisks)
            {
                log.Info("get_harddisks name is: " + item.Name);
                log.Info("get_harddisks capacity is: " + item.Capacity);
                log.Info("get_harddisks model is: " + item.Model);
                log.Info("get_harddisks Manufacturer is: " + item.Manufacturer);
                log.Info("get_harddisks serial number is: " + item.SerialNo);
                Assert.IsNotNull(item);
            }
        }
    }
}
