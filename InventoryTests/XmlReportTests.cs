﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Inventory;
using log4net;

namespace InventoryTests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class XmlReportTests
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(XmlReportTests));

        [TestMethod]
        public void create_invalid_report_with_null_path()
        {
            InventoryInfo inventoryInfo = new InventoryInfo();
            Assert.IsFalse(inventoryInfo.Report(null));
        }

        [TestMethod]
        public void create_invalid_report_with_invalid_path()
        {
            InventoryInfo inventoryInfo = new InventoryInfo();
            Assert.IsFalse(inventoryInfo.Report(""));
        }

        [TestMethod]
        public void create_valid_report()
        {
            InventoryInfo inventoryInfo = new InventoryInfo();            
            Assert.IsTrue(inventoryInfo.Report("output.xml"));
        }
    }
}
