﻿using System.Collections.Generic;
using Inventory;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InventoryTests
{
    [TestClass]
    public class PciDeviceInfoQueryTests
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PciDeviceInfoQueryTests));

        [TestMethod]
        public void get_pci_devices()
        {
            List<PciDeviceType> pciDevices = new PciDeviceInfo().Devices;
            Assert.AreNotEqual(0, pciDevices.Count);
            foreach (var item in pciDevices)
            {
                log.Info("name is: " + item.Name);
                log.Info("driver is: " + item.Driver);
                log.Info("device ident number is: " + item.DeviceId);
                log.Info("device description is: " + item.Description);
                Assert.IsNotNull(item);
            }
        }
    }
}
