﻿using System.Collections.Generic;
using Inventory;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InventoryTests
{
    [TestClass]
    public class RamInfoQueryTests
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RamInfoQueryTests));

        [TestMethod]
        public void get_ramdisks()
        {
            List<RamDiskType> ramdisks = new RamInfo().Rams;
            Assert.AreNotEqual(0, ramdisks.Count);
            foreach (var item in ramdisks)
            {
                log.Info("get_ramdisks name is: " + item.Name);
                log.Info("get_ramdisks capacity is: " + item.Capacity);
                log.Info("get_ramdisks model is: " + item.Model);
                log.Info("get_ramdisks Manufacturer is: " + item.Manufacturer);
                log.Info("get_ramdisks serial number is: " + item.SerialNo);
                log.Info("get_ramdisks part number number is: " + item.PartNumber);
                Assert.IsNotNull(item);
            }
        }
    }
}
