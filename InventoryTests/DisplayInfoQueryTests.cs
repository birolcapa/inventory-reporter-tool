﻿using Inventory;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace InventoryTests
{
    [TestClass]
    public class DisplayInfoQueryTests
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DisplayInfoQueryTests));

        private static List<DisplayType> displays;

        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            displays = new DisplayInfo().Displays;
        }

        [TestMethod]
        public void get_displays()
        {
            Assert.AreNotEqual(0, displays.Count);
            foreach (var item in displays)
            {
                log.Info("get_displays Display model is: " + item.Model);
                log.Info("get_displays Display serial number is: " + item.SerialNo);
                Assert.IsNotNull(item);
            }
        }

        [TestMethod]
        public void get_display_model()
        {
            Assert.AreNotEqual(0, displays.Count);
            string actual = displays[0].Model;
            log.Info("Display model is: " + actual);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void get_display_serial_number()
        {
            Assert.AreNotEqual(0, displays.Count);
            string actual = displays[0].SerialNo;
            log.Info("Display serial number is: " + actual);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void get_display_edid_number()
        {
            Assert.AreNotEqual(0, displays.Count);
            string actual = displays[0].FullEdid;
            log.Info("Display FullEdid is: " + actual);
            Assert.IsNotNull(actual);
        }
    }
}
