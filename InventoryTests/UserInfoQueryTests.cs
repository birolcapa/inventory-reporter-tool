﻿using Inventory;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InventoryTests
{
    [TestClass]
    public class UserInfoQueryTests
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UserInfoQueryTests));

        [TestMethod]
        public void get_user_name()
        {
            string actual = new UserInfo().Name;
            log.Info("User name is: " + actual);
            Assert.IsNotNull(actual);
        }
    }
}
