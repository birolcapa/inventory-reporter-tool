﻿using log4net;
using Inventory;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InventoryTests
{
    [TestClass]
    public class ComputerInfoQueryTests
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ComputerInfoQueryTests));

        [TestMethod]
        public void get_computer_name()
        {
            string actual = new ComputerInfo().Name;
            log.Info("Computer name is: " + actual);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void get_computer_model()
        {
            string actual = new ComputerInfo().Model;
            log.Info("Computer model is: " + actual);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void get_ident_no()
        {
            string actual = new ComputerInfo().IdentNo;
            log.Info("Computer ident no is: " + actual);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void get_manufacturer()
        {
            string actual = new ComputerInfo().Manufacturer;
            log.Info("Computer manufacturer is: " + actual);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void get_computer_info()
        {
            ComputerInfo computerInfo = new ComputerInfo();
            ComputerInfo actual = computerInfo;
            log.Info("Computer IdentNo is: " + actual.IdentNo);
            Assert.IsNotNull(actual.IdentNo);
            log.Info("Computer Manufacturer is: " + actual.Manufacturer);
            Assert.IsNotNull(actual.Manufacturer);
            log.Info("Computer Model is: " + actual.Model);
            Assert.IsNotNull(actual.Model);
            log.Info("Computer Name is: " + actual.Name);
            Assert.IsNotNull(actual.Name);
        }
    }
}
