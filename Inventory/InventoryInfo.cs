﻿using log4net;
using System.Runtime.Serialization;

namespace Inventory
{
    [DataContract]
    public class InventoryInfo : IReport
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(InventoryInfo));

        public InventoryInfo()
        {
            User = new UserInfo();
            Computer = new ComputerInfo();
            Display = new DisplayInfo();
            HardDisk = new HardDiskInfo();
            PciDevice = new PciDeviceInfo();
            Ram = new RamInfo();
        }

        [DataMember]
        public UserInfo User { get; internal set; }

        [DataMember]
        public ComputerInfo Computer { get; internal set; }

        [DataMember]
        public DisplayInfo Display { get; internal set; }

        [DataMember]
        public HardDiskInfo HardDisk { get; internal set; }

        [DataMember]
        public PciDeviceInfo PciDevice { get; internal set; }

        [DataMember]
        public RamInfo Ram { get; internal set; }

        /// <summary>
        /// Reports inventory to a file.
        /// Report file type depends on the extension of path:
        /// .xml (default) or .xlsx
        /// </summary>
        /// <param name="path">Input path</param>
        /// <returns></returns>
        public bool Report(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                log.Error("Reporting path must not be null or empty");
                return false;
            }
            IReport reporter = ReportFactory.Get(this, path);
            return reporter.Report(path);
        }
    }
}