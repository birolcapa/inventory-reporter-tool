﻿using log4net;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace Inventory
{
    internal class XmlReport : IReport
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(XmlReport));

        private InventoryInfo inventoryInfo;

        public XmlReport(InventoryInfo inventoryInfo)
        {
            this.inventoryInfo = inventoryInfo;
        }

        public bool Report(string path)
        {
            return SaveToFile(path);
        }

        private bool SaveToFile(string fileName)
        {
            try
            {
                WriteToFile(fileName);
                return true;
            }
            catch (Exception e)
            {
                log.Error(e);
                return false;
            }
        }

        private string FormatXml(string xml)
        {
            try
            {
                XDocument doc = XDocument.Parse(xml);
                return doc.ToString();
            }
            catch (Exception e)
            {
                log.Error(e);
                return xml;
            }
        }

        private void WriteToFile(string fileName)
        {
            StreamWriter streamWriter = null;
            try
            {
                string xmlString = FormatXml(Serialize());
                FileInfo xmlFile = new FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                streamWriter.Close();
            }
            finally
            {
                if (streamWriter != null)
                {
                    streamWriter.Dispose();
                }
            }
        }

        private string Serialize()
        {
            using (MemoryStream memoryStream = new MemoryStream())
            using (StreamReader reader = new StreamReader(memoryStream))
            {
                DataContractSerializer serializer = new DataContractSerializer(inventoryInfo.GetType());
                serializer.WriteObject(memoryStream, inventoryInfo);
                memoryStream.Position = 0;
                return reader.ReadToEnd();
            }
        }
    }
}
