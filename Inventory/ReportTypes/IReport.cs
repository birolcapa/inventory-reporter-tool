﻿namespace Inventory
{
    internal interface IReport
    {
        bool Report(string path);
    }
}