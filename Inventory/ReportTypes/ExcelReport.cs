﻿using log4net;
using SpreadsheetLight;
using System;
using System.IO;

namespace Inventory
{
    internal class ExcelReport : IReport
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ExcelReport));

        private InventoryInfo inventoryInfo;

        public ExcelReport(InventoryInfo inventoryInfo)
        {
            this.inventoryInfo = inventoryInfo;
        }

        public bool Report(string path)
        {
            bool isHeaderOk = WriteOutputExcelFileHeader(path);
            if (!isHeaderOk) return false;
            return WriteOutputExcelFile(path);
        }

        private bool WriteOutputExcelFileHeader(string path)
        {
            bool isheaderOk = false;
            if (!File.Exists(path))
            {
                SLDocument sl = new SLDocument();
                sl.SaveAs(path);
            }
            try
            {
                // SpreadsheetLight works on the idea of a currently selected worksheet.
                // If no worksheet name is provided on opening an existing spreadsheet,
                // the first available worksheet is selected.
                SLDocument sl = new SLDocument(path, "Sheet1");

                int rowNumber = 1;
                int columnIndex = 1;
                sl.SetCellValue(rowNumber, columnIndex++, "User");
                sl.SetCellValue(rowNumber, columnIndex++, "Computer");
                sl.SetCellValue(rowNumber, columnIndex++, "Display");
                sl.SetCellValue(rowNumber, columnIndex++, "HardDisk");
                sl.SetCellValue(rowNumber, columnIndex++, "Ram");
                sl.SetCellValue(rowNumber, columnIndex, "PciDevice");

                sl.Filter(1, 1, rowNumber, columnIndex);
                sl.SaveAs(path);
                isheaderOk = true;
            }
            catch (Exception e)
            {
                log.ErrorFormat("The file is used by another process: '{0}'", e.Message);
            }
            return isheaderOk;
        }

        private bool WriteOutputExcelFile(string path)
        {
            bool isExcelOk = false;
            try
            {
                SLDocument sl = new SLDocument(path, "Sheet1");

                int rowNumber = 2;
                int columnIndex = 1;

                sl.SetCellValue(rowNumber, columnIndex, inventoryInfo.User.Name);
                columnIndex++;

                sl.SetCellValue(rowNumber, columnIndex, inventoryInfo.Computer.Name);
                sl.SetCellValue(rowNumber++, columnIndex, inventoryInfo.Computer.Model);
                sl.SetCellValue(rowNumber++, columnIndex, inventoryInfo.Computer.Manufacturer);
                sl.SetCellValue(rowNumber++, columnIndex, inventoryInfo.Computer.IdentNo);

                rowNumber = 2;
                columnIndex++;

                for (int i = 0; i < inventoryInfo.Display.Displays.Count; ++i)
                {
                    DisplayType x = inventoryInfo.Display.Displays[i];
                    sl.SetCellValue(rowNumber++, columnIndex, "Display No: " + (i+1));
                    sl.SetCellValue(rowNumber++, columnIndex, x.Model);
                    sl.SetCellValue(rowNumber++, columnIndex, x.SerialNo);
                }

                rowNumber = 2;
                columnIndex++;

                for (int i = 0; i < inventoryInfo.HardDisk.HardDisks.Count; i++)
                {
                    HardDiskType x = inventoryInfo.HardDisk.HardDisks[i];
                    sl.SetCellValue(rowNumber++, columnIndex, "Hdd No: " + (i + 1));
                    sl.SetCellValue(rowNumber++, columnIndex, x.Capacity);
                    sl.SetCellValue(rowNumber++, columnIndex, x.Manufacturer);
                    sl.SetCellValue(rowNumber++, columnIndex, x.Model);
                    sl.SetCellValue(rowNumber++, columnIndex, x.Name);
                    sl.SetCellValue(rowNumber++, columnIndex, x.SerialNo);
                }

                rowNumber = 2;
                columnIndex++;

                for (int i = 0; i < inventoryInfo.Ram.Rams.Count; i++)
                {
                    RamDiskType x = inventoryInfo.Ram.Rams[i];
                    sl.SetCellValue(rowNumber++, columnIndex, "Ram No: " + (i + 1));
                    sl.SetCellValue(rowNumber++, columnIndex, x.Capacity);
                    sl.SetCellValue(rowNumber++, columnIndex, x.Manufacturer);
                    sl.SetCellValue(rowNumber++, columnIndex, x.Model);
                    sl.SetCellValue(rowNumber++, columnIndex, x.Name);
                    sl.SetCellValue(rowNumber++, columnIndex, x.PartNumber);
                    sl.SetCellValue(rowNumber++, columnIndex, x.SerialNo);
                }

                rowNumber = 2;
                columnIndex++;

                for (int i = 0; i < inventoryInfo.PciDevice.Devices.Count; i++)
                {
                    PciDeviceType x = inventoryInfo.PciDevice.Devices[i];
                    sl.SetCellValue(rowNumber++, columnIndex, "Pci No: " + (i + 1));
                    sl.SetCellValue(rowNumber++, columnIndex, x.Name);
                    // sl.SetCellValue(rowNumber++, columnIndex, x.Description);
                    // sl.SetCellValue(rowNumber++, columnIndex, x.DeviceId);
                    // sl.SetCellValue(rowNumber++, columnIndex, x.Driver);
                }

                SLWorksheetStatistics stats = sl.GetWorksheetStatistics();

                sl.AutoFitColumn(stats.StartColumnIndex, stats.EndColumnIndex);
                sl.AutoFitRow(stats.StartRowIndex, stats.EndRowIndex);
                //sl.SetRowHeight(stats.StartRowIndex, stats.EndRowIndex, 50);
                //sl.SetColumnWidth(stats.StartColumnIndex, stats.EndColumnIndex, 40);

                SLStyle style = sl.CreateStyle();
                style.SetWrapText(true);

                for (int i = 0; i < columnIndex; i++)
                {
                    sl.SetCellStyle(rowNumber, i, style);
                }

                sl.SaveAs(path);
                isExcelOk = true;
            }
            catch (Exception e)
            {
                log.ErrorFormat("The file is used by another process: '{0}'", e.Message);
            }
            return isExcelOk;
        }
    }
}