﻿using System.IO;

namespace Inventory
{
    static class ReportFactory
    {
        public static IReport Get(InventoryInfo inventoryInfo, string path)
        {
            string extension = Path.GetExtension(path);
            if (extension == ".xml")
            {
                return new XmlReport(inventoryInfo);
            }
            else if (extension == ".xlsx")
            {
                return new ExcelReport(inventoryInfo);
            }
            else
            {
                return new XmlReport(inventoryInfo);
            }
        }
    }
}