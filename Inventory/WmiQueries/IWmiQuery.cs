﻿using System.Collections.Generic;

namespace Inventory
{
    internal interface IWmiQuery
    {
        string DoQuery(string scope, string queryString, string key);
        List<IDeviceType> DoGenericQueries<IDeviceType>(string scope, string queryString);
    }
}