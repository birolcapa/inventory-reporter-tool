using log4net;
using System;
using System.Collections.Generic;
using System.Management;

namespace Inventory
{
    internal class WmiQuery : IWmiQuery
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WmiQuery));

        public string DoQuery(string scope, string queryString, string key)
        {
            string result = string.Empty;
            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher(scope, queryString);

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    result = queryObj[key].ToString();
                }
            }
            catch (ManagementException e)
            {
                log.Error("An error occurred while querying for WMI data: " + e.Message);
            }
            return result;
        }

        public List<T> DoGenericQueries<T>(string scope, string queryString)
        {
            List<T> items = new List<T>();
            try
            {
                ManagementObjectSearcher searcher = new
                    ManagementObjectSearcher(scope, queryString);

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    T item = CreateDiskTypeGeneric<T>(queryString, queryObj);
                    items.Add(item);
                }
            }
            catch (ManagementException e)
            {
                log.Error("An error occurred while querying for WMI data: " + e.Message);
            }
            return items;
        }

        private T CreateDiskTypeGeneric<T>(string queryString, ManagementObject queryObj)
        {
            // Ram
            if (queryString.Contains("Win32_PhysicalMemory"))
            {
                IDiskType ram = new RamDiskType
                {
                    Name = GetQueryObj(queryObj, "Name"),
                    Capacity = GetQueryObj(queryObj, "Capacity"),
                    Manufacturer = GetQueryObj(queryObj, "Manufacturer"),
                    Model = GetQueryObj(queryObj, "Model"),
                    SerialNo = GetQueryObj(queryObj, "SerialNumber"),
                    PartNumber = GetQueryObj(queryObj, "PartNumber")
                };
                return (T)Convert.ChangeType(ram, typeof(T));
            }
            else if (queryString.Contains("Win32_DiskDrive"))// hdd
            {
                IDiskType hd = new HardDiskType
                {
                    Name = GetQueryObj(queryObj, "Name"),
                    Capacity = GetQueryObj(queryObj, "Size"),
                    Manufacturer = GetQueryObj(queryObj, "Manufacturer"),
                    Model = GetQueryObj(queryObj, "Model"),
                    SerialNo = GetQueryObj(queryObj, "SerialNumber")
                };
                return (T)Convert.ChangeType(hd, typeof(T));
            }
            else
            {
                PciDeviceType pciDevice = new PciDeviceType
                {
                    Name = GetQueryObj(queryObj, "Caption"),
                    Driver = GetQueryObj(queryObj, "Service"),
                    DeviceId = GetQueryObj(queryObj, "DeviceID"),
                    Description = GetQueryObj(queryObj, "Description")
                };
                return (T)Convert.ChangeType(pciDevice, typeof(T));
            }
        }

        private static string GetQueryObj(ManagementObject queryObj, string key)
        {
            return queryObj[key] == null ? "Empty"
                : string.IsNullOrEmpty(queryObj[key].ToString().Trim()) ? "Empty"
                : queryObj[key].ToString().Trim();
        }
    }
}
