﻿using System.Runtime.Serialization;

namespace Inventory
{
    [DataContract]
    public class HardDiskType : IDiskType
    {
        [DataMember]
        public string Name { get; internal set; }

        [DataMember]
        public string Capacity { get; internal set; }

        [DataMember]
        public string Manufacturer { get; internal set; }

        [DataMember]
        public string Model { get; internal set; }

        [DataMember]
        public string SerialNo { get; internal set; }
    }
}