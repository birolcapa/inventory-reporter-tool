﻿namespace Inventory
{
    internal interface IDiskType : IDeviceType
    {
        string Capacity { get; }
        string Manufacturer { get; }
        string Model { get; }
        string SerialNo { get; }
    }
}
