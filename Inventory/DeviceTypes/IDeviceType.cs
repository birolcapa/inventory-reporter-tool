﻿namespace Inventory
{
    internal interface IDeviceType
    {
        string Name { get; }
    }
}
