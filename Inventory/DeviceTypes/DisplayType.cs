﻿using System.Runtime.Serialization;

namespace Inventory
{
    [DataContract]
    public class DisplayType
    {
        [DataMember]
        public string Model { get; internal set; }

        [DataMember]
        public string SerialNo { get; internal set; }

        public string FullEdid { get; internal set; }
    }
}