﻿using System.Runtime.Serialization;

namespace Inventory
{
    [DataContract]
    public class PciDeviceType : IDeviceType
    {
        [DataMember]
        public string Name { get; internal set; }

        [DataMember]
        public string Driver { get; internal set; }

        [DataMember]
        public string DeviceId { get; internal set; }

        [DataMember]
        public string Description { get; internal set; }
    }
}