﻿using System.Runtime.Serialization;

namespace Inventory
{
    [DataContract]
    public class ComputerInfo
    {
        public ComputerInfo()
        {
            IWmiQuery req = new WmiQuery();
            Name = req.DoQuery("root\\CIMV2", "SELECT * FROM Win32_ComputerSystem", "Name");
            Model = req.DoQuery("root\\CIMV2", "SELECT * FROM Win32_ComputerSystem", "Model");
            IdentNo = req.DoQuery("root\\CIMV2", "SELECT * FROM Win32_ComputerSystemProduct", "IdentifyingNumber");
            Manufacturer = req.DoQuery("root\\CIMV2", "SELECT * FROM Win32_ComputerSystem", "Manufacturer");
        }

        [DataMember]
        public string Name { get; internal set; }

        [DataMember]
        public string Model { get; internal set; }

        [DataMember]
        public string IdentNo { get; internal set; }

        [DataMember]
        public string Manufacturer { get; internal set; }
    }
}