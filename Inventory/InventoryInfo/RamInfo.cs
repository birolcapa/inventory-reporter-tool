﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Inventory
{
    [DataContract]
    public class RamInfo
    {
        public RamInfo()
        {
            IWmiQuery req = new WmiQuery();
            Rams = req.DoGenericQueries<RamDiskType>("root\\CIMV2", "SELECT * FROM Win32_PhysicalMemory");
        }

        [DataMember]
        public List<RamDiskType> Rams { get; internal set; }
    }
}