﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Inventory
{
    [DataContract]
    public class HardDiskInfo
    {
        public HardDiskInfo()
        {
            IWmiQuery req = new WmiQuery();
            HardDisks = req.DoGenericQueries<HardDiskType>("root\\CIMV2", "SELECT * FROM Win32_DiskDrive");
        }

        [DataMember]
        public List<HardDiskType> HardDisks { get; internal set; }
    }
}