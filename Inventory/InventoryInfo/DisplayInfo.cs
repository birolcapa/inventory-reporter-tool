﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Inventory
{
    [DataContract]
    public class DisplayInfo
    {
        public DisplayInfo()
        {
            Displays = DisplayEdidUtil.GetDisplays();
        }

        [DataMember]
        public List<DisplayType> Displays { get; set; }
    }
}
