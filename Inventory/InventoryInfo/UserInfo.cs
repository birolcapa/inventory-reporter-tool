﻿using System;
using System.Runtime.Serialization;

namespace Inventory
{
    [DataContract]
    public class UserInfo
    {
        public UserInfo()
        {
            Name = Environment.GetEnvironmentVariable("UserName");
        }

        [DataMember]
        public string Name { get; internal set; }
    }
}