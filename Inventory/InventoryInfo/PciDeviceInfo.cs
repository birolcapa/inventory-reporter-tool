﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Inventory
{
    [DataContract]
    public class PciDeviceInfo
    {
        public PciDeviceInfo()
        {
            IWmiQuery req = new WmiQuery();
            Devices = req.DoGenericQueries<PciDeviceType>("root\\CIMV2",
                        "SELECT * FROM Win32_PnPEntity WHERE DeviceID LIKE 'PCI%'");
        }

        [DataMember]
        public List<PciDeviceType> Devices { get; internal set; }
    }
}